from decouple import config
from kafka import KafkaConsumer, KafkaProducer
#from multiprocessing import Process
import concurrent.futures
from azuredevopsX import factories
from pprint import pprint
import json
import datetime
import pickle

class Util:
    
    def __json_default(self,value):
        """ Convert a specific object to dict 
        
        Args:
            object value: a object to convert to dict
        """
        if isinstance(value, datetime.date):
            return dict(year=value.year, month=value.month, day=value.day)
        else:
            return value.__dict__

    def object_to_json(self, entity):
        return json.dumps(entity,ensure_ascii=False,default = lambda o: self.__json_default(o), sort_keys=True, indent=4).encode('utf-8')

    def object_to_dict(self, entity):
        """ Convert an object to dict 
        
        Args:
            object entity: a entity to convert to dict
        """
        json_entity = self.object_to_json(entity)
        return json.loads(json_entity)



consumer = KafkaConsumer(
    config('TOPIC'),
    group_id=config('GROUP_ID'),
    bootstrap_servers=config('SERVERS'),
    auto_offset_reset='earliest',  
    enable_auto_commit=True,  
    value_deserializer=lambda x: json.loads(x.decode('utf-8'))
    )

util = Util()

def publish (**kwargs):
    
    pprint ("topic {}".format(kwargs["topic"]))
    pprint ("-"*50)
    
    data = util.object_to_dict(kwargs["data"])

    producer = KafkaProducer(bootstrap_servers=config('SERVERS'),value_serializer=lambda v: json.dumps(v).encode('utf-8'))
    producer.send(kwargs["topic"], data)
    producer.flush()

def extract_function(project,extract):
    for key, value in extract.items():
        value.get_by_project_function(project.id,function=publish, topic="application.{}.{}".format("msazuredevops",key), extra_data = None)       


try:
    for message in consumer:
        
        mensagem = message.value
        
        personal_access_token  = mensagem["secret"]
        organization_url = mensagem["organizational_url"]

        project_factory = factories.ProjectFactory(personal_access_token=personal_access_token, organization_url=organization_url)
        projects = project_factory.get_all_function(function=publish, topic="application.{}.{}".format("msazuredevops","project"), extra_data = None)  
        
        project_dict = []
        
        extract = {
            
            'interaction': factories.InteractionFactory(personal_access_token=personal_access_token,
                                                        organization_url=organization_url),
            
            'team': factories.TeamFactory(personal_access_token=personal_access_token,
                                                        organization_url=organization_url), 
            
            'teammember': factories.TeamMemberFactory(personal_access_token=personal_access_token,
                                                        organization_url=organization_url),
            
            'workitem': factories.WorkItemFactory(personal_access_token=personal_access_token,
                                                        organization_url=organization_url),

            'workitemhistory': factories.WorkItemHistoryFactory(personal_access_token=personal_access_token,
                                                        organization_url=organization_url), 
            
            'backlog': factories.WorkItemFactory(personal_access_token=personal_access_token,
                                                        organization_url=organization_url), 
                                                        
            'workitemtype': factories.WorkItemFactory(personal_access_token=personal_access_token,
                                                        organization_url=organization_url)

            }

        for project in projects:
            extract_function(project, extract=extract)
        


except KeyboardInterrupt:
    print ("Interrupção do teclado detectada. Encerrando o consumidor");
finally:
    consumer.close()