from kafka import KafkaProducer
from decouple import config
import json
try:
        producer = KafkaProducer(bootstrap_servers=config('SERVERS'),value_serializer=lambda v: json.dumps(v).encode('utf-8'))
        data = {  
                "secret":config('SECRET'),
                "organizational_url": config('URL'),
                "configuration_uuid": config('CONFIGURATION_UUID'),
                "organization_uuid": config('ORGANIZATION_UUID')
                }
        producer.send('extract_devops', data)
        producer.flush()
        print ("send {}".format (config('SERVERS')))
except:
        print ("erro")