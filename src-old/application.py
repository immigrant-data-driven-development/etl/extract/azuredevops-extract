import apache_beam as beam
from beam_nuggets.io import kafkaio
from decouple import config
from services.transform import TransformPublish, TransformRetrieveProject, TransformByProjectEntity
import logging
from apache_beam.options.pipeline_options import PipelineOptions

logging.basicConfig(level=logging.INFO)


consumer_config = {"topic": config('TOPIC'),
                   "bootstrap_servers": config('SERVERS'),
                   "group_id": config('GROUP_ID'),}


def run_pipeline():
    pipeline_options = PipelineOptions(
        runner="DirectRunner",  # Or other runner like "DataflowRunner"
        num_workers=10  # Specify the desired number of workers
    )


    with beam.Pipeline(options=pipeline_options) as pipeline:


        credential = (pipeline|  "Reading messages from Kafka"  >> kafkaio.KafkaConsume(
                                            consumer_config=consumer_config))
        
        projects = credential | "Extract Data Project " >> TransformRetrieveProject() 
        
        projects | "Publish Project Information" >> TransformPublish(entity="project") 

        projects | "Extract Data interaction from project and Send To Kafka: interaction" >> TransformByProjectEntity('interaction')

        projects | "Extract Data team from project and Send To Kafka: team" >> TransformByProjectEntity('team')

        projects | "Extract Data teammember from project and Send To Kafka: teammember" >> TransformByProjectEntity('teammember')

        projects | "Extract Data workitem from project and Send To Kafka: workitem" >> TransformByProjectEntity('workitem')

        projects | "Extract Data backlog from project and Send To Kafka: backlog" >> TransformByProjectEntity('backlog')
        
        projects | "Extract Data backlog from workitemtype and Send To Kafka: workitemtype" >> TransformByProjectEntity('workitemtype')
        
        projects | "Extract Data backlog from workitemhistory and Send To Kafka: workitemhistory" >> TransformByProjectEntity('workitemhistory')

if __name__ == "__main__":
    run_pipeline()   


    