from apache_beam.transforms import PTransform
import apache_beam as beam
from .fn import FnExtract, FnTuple, FnExtractByProject
from beam_nuggets.io import kafkaio
from decouple import config


class TransformRetrieveProject(PTransform):
    def __init__(self):
        self.application = "msazuredevops"
        self.entities = ['project']
        self.producer_config = {'bootstrap.servers': config('SERVERS')}

    def expand(self, pcoll):
        result = None
        for entity in self.entities:
            result = (
                pcoll
                | "Retrieve {}".format(entity) >>beam.ParDo(FnExtract(function_name=entity))    
            )
                
        return result

class TransformPublish(PTransform):
    def __init__(self, entity = None):
        self.application = "msazuredevops"
        self.producer_config = {'bootstrap.servers': config('SERVERS')}
        self.entity = entity
    def expand(self, pcoll):
        result = (
                pcoll
                | "Transform in Tuple {}".format( self.entity) >>beam.ParDo(FnTuple())
                | "Sending  {} to Kafka".format( self.entity) >> kafkaio.KafkaProduce(topic="application.{}.{}".format(self.application, self.entity),servers=config('SERVERS'))        
            )
                
        return result


class TransformByProjectEntity(PTransform):
     
    def __init__(self, entity, details = False):
        self.application = "msazuredevops"
        self.entity = entity
        self.details= details
        self.producer_config = {'bootstrap.servers': config('SERVERS')}

    def expand(self, pcoll):
       
        result = (
                pcoll
                | "Retrive all {} from a project".format(self.entity) >>beam.ParDo(FnExtractByProject(function_name=self.entity,details= self.details)) 
                | "Transform in Tuple {} from a project".format(self.entity) >>beam.ParDo(FnTuple())
                | "Sending  {} to Kafka from a project".format(self.entity) >> kafkaio.KafkaProduce(topic="application.{}.{}".format(self.application,self.entity),servers=config('SERVERS'))        
         )
                
        return result

class Transform(PTransform):
    def __init__(self, entity):
        self.application = "msazuredevops"
        self.entity = entity       
        self.producer_config = {'bootstrap.servers': "config('SERVERS')"}
        
    def expand(self, pcoll):
        result = (
             pcoll
            | "Retrieve {}".format(self.entity) >>beam.ParDo(FnExtract(function_name=self.entity))
            | "Transform in Tuple {}".format(self.entity) >>beam.ParDo(FnTuple())
            | "Sending  {} to Kafka".format(self.entity) >> kafkaio.KafkaProduce(topic="application.{}.{}".format(self.application,self.entity),servers=config('SERVERS'))
                
        )
                  
        return result


